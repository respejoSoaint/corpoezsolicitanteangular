import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RegistroComponent } from './component/registro/registro.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { Error404Component } from './component/error404/error404.component';
import { LoginComponent } from './component/login/login.component';
import { InicioComponent } from './component/inicio/inicio.component';
import { ConsultaComponent } from './component/guias/consulta/consulta.component';
import { SolicitudComponent } from './component/guias/solicitud/solicitud.component';
import { VehiculosComponent } from './component/recursos/vehiculos/vehiculos.component';
import { ConductoresComponent } from './component/recursos/conductores/conductores.component';

const routes: Routes = [
  { path: '' , component: InicioComponent, pathMatch: 'full' },
  { path: 'inicio' , component: InicioComponent, pathMatch: 'full' },
  { path: 'login' , component: LoginComponent, pathMatch: 'full' },
  { path: 'registrar' , component: RegistroComponent, pathMatch: 'full' },
  { path: 'dashboard' , component: DashboardComponent , 
      children:  
        [
          { path: 'guias/consulta' , component: ConsultaComponent , pathMatch: 'full' },
          { path: 'guias/solicitud' , component: SolicitudComponent , pathMatch: 'full' },
          { path: 'recursos/vehiculos' , component: VehiculosComponent , pathMatch: 'full' },
          { path: 'recursos/conductores' , component: ConductoresComponent , pathMatch: 'full' },
          { path: 'logout/:id' , component: LoginComponent },
        ]
  },
  { path: '404' , component: Error404Component },  
  { path: '**' , redirectTo: '404' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
