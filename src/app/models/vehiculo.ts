export class Vehiculo{
    constructor (        
        public marca : string,
        public modelo : string,
        public color: string,
        public placa : string,        
        public batea: string,
    ){}
}