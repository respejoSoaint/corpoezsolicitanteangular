export class Usuarios{
    constructor (        
        public correo : string,
        public clave : string,
        public idrol: number,
        public rol : string,        
        public nombres: string,
        public apellidos: string,
        public rif: string,
        public razonsocial: string,
        public responsable: string

    ){}
}