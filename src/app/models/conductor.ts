export class Conductor{
    constructor (        
        public cedula : string,
        public nombres : string,
        public apellidos: string,
        public telefono : string,        
    ){}
}