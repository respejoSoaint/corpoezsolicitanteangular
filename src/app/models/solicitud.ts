export class Solicitud{
    constructor (        
        public id : number,
        public numero : string,
        public guia: number,
        public origen : string,        
        public destino: string,
        public conductor: string,
        public vehiculo: string,
        public material: string,
        public peso: string,
        public costo: string,
        public status: string,

    ){}
}