import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SolicitudesService {

  constructor(
    private _http : Http
  ) { }

  getSolicitudes(data): Observable<any>{
    let json = JSON.stringify(data);
    let params = 'json='+json;
    //let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this._http.request('assets/json/solicitudes.json', params);    
  } 


}
