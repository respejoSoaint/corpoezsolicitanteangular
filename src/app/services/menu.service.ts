import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Http, Response } from '@angular/http';
//import { GLOBAL } from './global';

@Injectable({
  providedIn: 'root'
})
export class MenuService {
  public url: string;
  public errorHttp: boolean;

  constructor( 
    public _http : Http 
  
  ){ 
      //this.url = GLOBAL.url;
  }
  
  getMenu(data): Observable<any>{
    let json = JSON.stringify(data);
    let params = 'json='+json;
    //let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this._http.request('assets/json/menu.json', params);    
  }
  
}
