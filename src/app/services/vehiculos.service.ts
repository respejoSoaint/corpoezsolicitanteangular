import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VehiculosService {

  constructor(
    private _http : Http
  ) { }

  getVehiculos(data): Observable<any>{
    let json = JSON.stringify(data);
    let params = 'json='+json;
    //let headers = new HttpHeaders().set('Content-Type','application/x-www-form-urlencoded');
    return this._http.request('assets/json/vehiculos.json', params);    
  }

  postVehiculos(form){

    let request = this._http.request('assets/json/vehiculos.json');
  
  }


}



