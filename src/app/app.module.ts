import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http'; 
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { RegistroComponent } from './component/registro/registro.component';
import { DashboardComponent } from './component/dashboard/dashboard.component';
import { Error404Component } from './component/error404/error404.component';
import { LoginComponent } from './component/login/login.component';
import { InicioComponent } from './component/inicio/inicio.component';
import { RegistroEmpresaComponent } from './component/registro-empresa/registro-empresa.component';
import { VehiculosComponent } from './component/recursos/vehiculos/vehiculos.component';
import { ConductoresComponent } from './component/recursos/conductores/conductores.component';
import { ConsultaComponent } from './component/guias/consulta/consulta.component';
import { SolicitudComponent } from './component/guias/solicitud/solicitud.component';

@NgModule({
  declarations: [
    AppComponent,
    RegistroComponent,
    DashboardComponent,
    Error404Component,
    LoginComponent,
    InicioComponent,
    RegistroEmpresaComponent,
    VehiculosComponent,
    ConductoresComponent,
    ConsultaComponent,
    SolicitudComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
