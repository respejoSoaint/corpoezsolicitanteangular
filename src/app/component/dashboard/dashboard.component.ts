import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuService } from 'src/app/services/menu.service';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  public sidebar:any;
  public identity;
  public token;
  public menu: any;
  public show: boolean;

  constructor(
    private _router: Router,
    private _menuService: MenuService,
    private _userService: UsuariosService
  ) { 

    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
  }

  ngOnInit() {

   
    if(this.token!=null && this.identity!=null){
      this.show = true;
      console.log(this.identity);
      
      this._menuService.getMenu(this.identity).subscribe(
        response => { this.menu = response.json() } 
      );
      
    }else{
      alert("Debe iniciar secion nuevamente");
      this._router.navigate(['/login']);
    }

  }

}
