import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Usuarios } from 'src/app/models/usuarios';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public title: string;
  public usuario : Usuarios;
  public mensaje : String;
  public token ;
  public identity ;
  public loading : boolean;

  constructor(
    private _userservice : UsuariosService,
    private _router : Router,
    private _route : ActivatedRoute
  ) {
    this.usuario = new Usuarios('','',null,'','','','','','');
   }

  ngOnInit() {
    this.logout();
    this.loading = false;
  }

  onSubmit(form){
    this.loading = true;
    this._userservice.signup(this.usuario).subscribe(
        response => {
          this.loading = true;
          
          if(response.status == 'error'){
            this.mensaje = response.message;
            this.loading = false;
          }else{
              //token
              let user = this.usuario.correo;
              let pass = this.usuario.clave;
              let data_usuarios = response.json();
              let valid = false;
              console.log(data_usuarios);
              for(var i=0; i < data_usuarios.length; i++){

                if(data_usuarios[i].correo==user && data_usuarios[i].clave==pass){
                  valid = true;
                  this.token = data_usuarios[i].correo+data_usuarios[i].clave ;
                  localStorage.setItem('token',this.token);
                  this.identity = data_usuarios[i];
                  localStorage.setItem('identity', JSON.stringify(this.identity)); 
           
              }

              if(valid==true){
                this._router.navigate(['/dashboard']);
              }else{
                this.mensaje="Usuario y clave erroneo";
              }

            }


        } 
      });
  }

  logout(){
    this._route.params.subscribe(params => {
      let logout = +params['id'];
      if(logout==1){
        localStorage.removeItem('identity');
        localStorage.removeItem('token');

        this.identity = null;
        this.token = null;
        this._router.navigate(['login']);
      }
    });
  }

}
