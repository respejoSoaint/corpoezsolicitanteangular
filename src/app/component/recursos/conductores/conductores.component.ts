import { Component, OnInit } from '@angular/core';
import { Conductor } from '../../../models/conductor';
import { ConductoresService } from 'src/app/services/conductores.service';
import { UsuariosService } from 'src/app/services/usuarios.service';

@Component({
  selector: 'app-conductores',
  templateUrl: './conductores.component.html',
  styleUrls: ['./conductores.component.css']
})
export class ConductoresComponent implements OnInit {

  public mensaje;
  public identity;
  public token;
  public conductor : Conductor;
  public conductoresArr: Array<any> = [] ;

  constructor(
      private _conductor : ConductoresService,
      private _userService: UsuariosService
  ) {
    this.conductor = new Conductor(null,'','','');
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
  }

  ngOnInit() {

    this._conductor.getConductores(this.identity.rif).subscribe(
      response => {
          let data_response = response.json();
          let valid = false;
          let error;
          for(let i=0 ; i < data_response.length ; i++){
              if(data_response[i].rif==this.identity.rif){
                valid = true;  
                this.conductoresArr.push(data_response[i]);
              }
          }

          if(valid==true && this.conductoresArr.length>0){
            this.conductoresArr;
          }else{
            error = "No existen datos";
          }
      }
    );
  }

  onSubmitConductor(form){

  }

}
