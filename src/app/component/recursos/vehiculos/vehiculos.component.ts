import { Component, OnInit } from '@angular/core';
import { Vehiculo } from '../../../models/vehiculo';
import { VehiculosService } from '../../../services/vehiculos.service';
import { UsuariosService } from 'src/app/services/usuarios.service';


@Component({
  selector: 'app-vehiculos',
  templateUrl: './vehiculos.component.html',
  styleUrls: ['./vehiculos.component.css']
})
export class VehiculosComponent implements OnInit {
  
  public mensaje;
  public identity;
  public token;
  public vehiculo : Vehiculo;
  public vehiculosArr: Array<any> = [] ;

  constructor(
    private _vehiculo : VehiculosService,
    private _userService: UsuariosService
  ) {
    this.vehiculo = new Vehiculo('','','','','');
    this.identity = this._userService.getIdentity();
    this.token = this._userService.getToken();
  }

  ngOnInit() {
    
    this._vehiculo.getVehiculos(this.identity.rif).subscribe(
      response => {
          let data_response = response.json();
          let valid = false;
          let error;
          for(let i=0 ; i < data_response.length ; i++){
              if(data_response[i].rif==this.identity.rif){
                valid = true;  
                this.vehiculosArr.push(data_response[i]);
              }
          }

          if(valid==true && this.vehiculosArr.length>0){
            this.vehiculosArr;
          }else{
            error = "No existen datos";
          }
      }
    );
       

  }

  onSubmitVehiculo(vehiculoForm){
      console.log(vehiculoForm);
  }

}
